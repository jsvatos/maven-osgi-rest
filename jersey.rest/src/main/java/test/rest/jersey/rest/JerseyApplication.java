

package test.rest.jersey.rest;

import test.rest.api.RestEndpoint;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.ws.rs.core.Application;

public class JerseyApplication extends Application {

    private Set<Class<?>> set = new HashSet<Class<?>>();
    
    
    public JerseyApplication(List<RestEndpoint> endpoints) {
        for (RestEndpoint endpoint : endpoints) {
            set.add(endpoint.getClass());
        }
    }

    @Override
    public Set<Class<?>> getClasses() {
return set;    }

}
