package test.rest.jersey.rest;

import test.rest.api.RestEndpoint;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.ws.rs.core.Application;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferenceCardinality;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.servlet.ServletContainer;
import org.osgi.framework.BundleContext;
import org.osgi.service.http.HttpService;
import org.osgi.service.http.NamespaceException;

/**
 *
 * @author z003ppru
 */
@Component
public class ServerComponent {

    @Reference(policy = ReferencePolicy.DYNAMIC, cardinality = ReferenceCardinality.OPTIONAL_MULTIPLE, referenceInterface = RestEndpoint.class)
    private List<RestEndpoint> endpoints = new ArrayList<RestEndpoint>();

    @Reference(referenceInterface = HttpService.class, policy = ReferencePolicy.STATIC, cardinality = ReferenceCardinality.MANDATORY_UNARY)
    private HttpService srv;

    //
    protected void unbindEndpoints(RestEndpoint r) {

    }

    protected void bindEndpoints(final RestEndpoint r) {
        log.info("New endpoint found " + r.getClass().getName());
        if (srv == null) {
            endpoints.add(r);
        } else {
            try {
                srv.registerServlet("/" + r.endpoint(), new ServletContainer(
                        ResourceConfig.forApplication(new Application() {
                            @Override
                            public Set<Class<?>> getClasses() {
                                return (Set<Class<?>>) Collections.singleton((Class<?>) r.getClass());
                            }
                        })), null, null);
                log.info("New endpoint was introduced as: http:///localhost:8080/" + r.endpoint());
        } catch (Throwable ex) {
                log.log(Level.SEVERE, null, ex);
            }
        }
    }
    private static final Logger log = Logger.getLogger(ServerComponent.class.getName());

    @Activate
    protected void bingHttpService(BundleContext bc) {
        System.out.println("Server-Client for REST started. http:///localhost:8080/");

        ClassLoader myClassLoader = getClass().getClassLoader();
        ClassLoader originalContextClassLoader = Thread.currentThread().getContextClassLoader();
        try {
            Thread.currentThread().setContextClassLoader(myClassLoader);

            srv.registerServlet("/", new ServletContainer(
                    ResourceConfig.forApplication(new JerseyApplication(endpoints))), null, null);
            for (RestEndpoint r : endpoints) {
                log.info("New endpoint was introduced as: http:///localhost:8080/" + r.endpoint());
            }
        } catch (Throwable ex) {
            log.log(Level.SEVERE, null, ex);
        }
    }

}
