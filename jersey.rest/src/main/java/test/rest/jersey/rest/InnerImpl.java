package test.rest.jersey.rest;

import test.rest.api.RestEndpoint;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

@Component
@Path("inner")
@Service(value = RestEndpoint.class)
public class InnerImpl implements RestEndpoint {

    @GET

    public Response getStatus() {
        return Response.ok().header("Access-Control-Allow-Origin", "*")
                .entity("inner OK").build();
    }

    public String endpoint() {
        return "inner";
    }
}
