package test.rest.extension.impl;

import test.rest.api.RestEndpoint;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

@Component
@Path("outter")
@Service(value = RestEndpoint.class)
public class OutterImpl implements RestEndpoint {

    @GET

    public Response getStatus() {
        return Response.ok().header("Access-Control-Allow-Origin", "*")
                .entity("outter OK").build();
    }

    public String endpoint() {
        return "outter";
    }
}
