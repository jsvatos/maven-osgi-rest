#!/bin/bash


bundles=""
bundles="$bundles,org.eclipse.equinox.console_1.1.200.v20150929-1405.jar@start"
bundles="$bundles,org.apache.felix.gogo.runtime_0.10.0.v201209301036.jar@start"
bundles="$bundles,org.apache.felix.gogo.command_0.10.0.v201209301215.jar@start"
bundles="$bundles,org.apache.felix.gogo.shell_0.10.0.v201212101605.jar@start"
bundles="$bundles,org.eclipse.equinox.ds_1.4.400.v20160226-2036.jar@2:start"
bundles="$bundles,org.eclipse.equinox.util_1.0.500.v20130404-1337.jar@start"
bundles="$bundles,org.eclipse.osgi.services-3.5.0.v20150519-2006.jar@start"
bundles="$bundles,../api/target/api-1.0.jar@start"
bundles="$bundles,../extensionImpl/target/extensionImpl-1.0.jar@start"
bundles="$bundles,../jersey.rest/target/jersey.rest-1.0.jar@start"




for entry in ./plugins/*.jar
do
	bundles="$bundles,$entry@start"
done

#bundles=${bundles:0:${#bundles}-1}


paramLimitMem="-Xms1G -Xmx1G"
 
paramOsgi="-Dosgi.bundles=$bundles -jar org.eclipse.osgi_3.11.0.v20160603-1336.jar -console -clean -Djavax.ws.rs.ext.RuntimeDelegate=org.apache.cxf.jaxrs.impl.RuntimeDelegateImpl"
#paramOsgi=" -jar org.eclipse.osgi_3.11.0.v20160603-1336.jar -console -clean"
paramDebug="-agentlib:jdwp=transport=dt_socket,server=y,suspend=y,address=6006"

#echo $paramLimitMem $paramOsgi 
#java $paramDebug $paramLimitMem $paramOsgi $redirectStderr $redirectStdout
#java -Dequinox.ds.debug=true -Dequinox.ds.print=true $paramLimitMem $paramOsgi 
java $paramLimitMem $paramOsgi 