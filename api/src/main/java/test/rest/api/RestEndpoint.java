
package test.rest.api;

/**
 *
 * @author jiri.svatos@gmail.com
 */
public interface RestEndpoint {
    String endpoint();
}
